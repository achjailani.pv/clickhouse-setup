FROM ubuntu:20.04

# install basic stuff including sudo
RUN apt-get update \
    && apt-get install -y xxd sudo tzdata supervisor vim \
    && ln -fs /usr/share/zoneinfo/UTC /etc/localtime \
    && dpkg-reconfigure -f noninteractive tzdata \
    && rm -rf /var/lib/apt/lists/*

ENV TZ="UTC"
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y apt-transport-https dirmngr ca-certificates
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 8919F6BD2B48D754
RUN echo "deb https://packages.clickhouse.com/deb stable main" | tee \
    /etc/apt/sources.list.d/clickhouse.list

RUN apt-get update && apt-get install -y \
    clickhouse-server=23.2.3.17  \
    clickhouse-client=23.2.3.17  \
    clickhouse-common-static=23.2.3.17

WORKDIR /app/src

COPY make.sh generator_script.sh ./

RUN mkdir generated_files && \
    chmod +x make.sh generator_script.sh

RUN ./generator_script.sh

RUN mv generated_files/custom_config.xml /etc/clickhouse-server/config.d/ && \
    chown clickhouse:clickhouse /etc/clickhouse-server/config.d/custom_config.xml

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN chmod +x /etc/supervisor/conf.d/supervisord.conf

EXPOSE 8123 9000

CMD ["supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]