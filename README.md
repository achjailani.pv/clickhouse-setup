# Clickhouse
> This page would explain step-by-step how to set up and configure **Clickhouse** server running on **Google Cloud Provider (GCP) VM** along with **Google Cloud Storage (GCS)** as storage

Before getting started, make sure you have a cup of coffee with you :)
### Table of Contents
- [Preparation](#preparation)
    - [Create VM](#create-vm)
    - [Create GCS Bucket](#create-gcs-bucket)
- [Installation](#installation)
- [Commands](#commands)
- [Configuration](#configuration)
    - [Main](#main)
    - [User](#user)
    - [Storage](#storage)
    - [GKE Cluster](#gke-cluster)
- [Firewall](#firewall)
- [Clickhouse client](#clickhouse-client)
- [Note](#note)
  - [JSON type](#json-type)
- [Docker](#docker)
- [Implementation](TUTORIAL.md)
- [Link](#link)


## Preparation
Before moving forward, there some essential things we should prepare

### Create VM
As we're going to run Clickhouse on GCP, the first thing we should create VM with the following requirements
* 4GB minimum RAM
* Debian GNU/Linux 10(buster) OS (recommended)

### Create GCS Bucket
In the use of Clickhouse, the disk we're going to use Google Cloud Storage (GCS).
make sure to create a GCS bucket and its folder for later use

## Installation
The installation process would take a few steps

* Setup debian repository
```shell
sudo apt-get -y update
sudo apt-get install -y apt-transport-https ca-certificates dirmngr
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 8919F6BD2B48D754
sudo echo "deb https://packages.clickhouse.com/deb stable main" | sudo tee \
/etc/apt/sources.list.d/clickhouse.list
```
* Install clickhouse server & client
```shell
sudo apt-get update
sudo apt-get install -y clickhouse-server=22.12.4.76 clickhouse-client=22.12.4.76 clickhouse-common-static=22.12.4.76
```
* Start clickhouse server
```shell
sudo service clickhouse-server start
```

## Commands
Start clickhouse server
```shell
sudo service clickhouse-server start
```

Stop server
```shell script
sudo service clickhouse-server stop
```
Check server status
```shell script
sudo service clickhouse-server status
```
Run client
```shell 
clickhouse-client --user default --password

# default user is: default
# --password if password exists
```

## Configuration
This step would walk us through how to configure Clickhouse server for our need.

### Main
`config.xml` is the main (default) configuration which located in `/etc/clickhouse-server/config.xml`. the default configuration server would only listen from local IP. uncomment `<listen_host>0.0.0.0</listen_host>` to make server listens from any IP

### User
`users.xml` is a user configuration, setting client password, etc.

### Storage
Create storage config
```shell
vim /etc/clickhouse-server/template.d/storage_config.xml
```
Place the following script

```xml
<clickhouse>
    <storage_configuration>
        <disks>
            <gcs>
                <support_batch_delete>false</support_batch_delete>
                <type>s3</type>
                <endpoint>https://storage.googleapis.com/GCS BUCKET/GCS BUCKET FOLDER/</endpoint>
                <access_key_id>ACCESS KEY ID</access_key_id>
                <secret_access_key>SECRET ACCESS KEY</secret_access_key>
                <metadata_path>/var/lib/clickhouse/disks/gcs/</metadata_path>
            </gcs>
            <cache>
                <type>cache</type>
                <disk>gcs</disk>
                <path>/var/lib/clickhouse/disks/gcs_cache/</path>
                <max_size>10Gi</max_size>
            </cache>
        </disks>
        <policies>
            <gcs_main>
                <volumes>
                    <main>
                        <disk>gcs</disk>
                    </main>
                </volumes>
            </gcs_main>
        </policies>
    </storage_configuration>
</clickhouse>
```
Change the owner
```shell
sudo chown clickhouse:clickhouse /etc/clickhouse-server/template.d/storage_config.xml
```
Lastly, restart the server
```shell
sudo service clickhouse-server restart
```
### GKE Cluster

## Firewall
To enable clickhouse accessed from external, the firewall should be prepared

Install firewall
```shell
sudo apt install ufw
```

Firewall status
```shell
sudo ufw status verbose
```

Allow ssh
```shell
sudo ufw allow ssh
```

Activate firewall
```shell
sudo ufw enable
```

```shell
sudo ufw allow 8123
sudo ufw allow 9000
```

## Clickhouse Client
Running clickhouse client with the following command.

```shell
clickhouse-client --host <HOSTNAME> \
                  --secure \
                  --port 9440 \
                  --user <USERNAME> \
                  --password <PASSWORD>
```

Verify disk configuration
```sql
SELECT * FROM system.disks FORMAT Vertical
```

Storage Policy
```sql
SELECT policy_name, volume_name, disks FROM system.storage_policies
```

## Note
The following things should be noticed
### JSON type
Clickhouse support JSON data type for experimental only, by default we could not set JSON data type, to do that we need to set `SET allow_experimental_object_type = 1`

```sql
SET allow_experimental_object_type = 1
```
```sql
CREATE TABLE foo
(
    id Int64,
  	`event` JSON,
    message String,
    created_at DateTime,
)
ENGINE = MergeTree()
PRIMARY KEY (id, timestamp)
```
Instead of `JSON` type which is not ready for production, it's better using `String` type
## Docker
The installation also could be done with `docker` container, where the `dockerfile` can be found [here](Dockerfile)

Create docker image
```shell
docker build --no-cache -t docker-clickhouse .
```

Run container
```shell
docker run -d --env-file=.env --name docker-clickhouse --publish 8123:8123 --publish 9004:9000 docker-clickhouse
```

Configure the storage by modified the GCS credential of file `/app/src/generated_files/custom_storage.xml` by filling some attributes below

```xml
<endpoint></endpoint>
<access_key_id></access_key_id>
<secret_access_key></secret_access_key>
```

Move configuration & change owner
```shell
mv generated_files/custom_storage.xml /etc/clickhouse-server/config.d/
chown clickhouse:clickhouse /etc/clickhouse-server/config.d/custom_storage.xml
```
Finally, restart the server

```shell
service clickhouse-server restart
```

Now, we can check the storage config

Connect to server

```shell
clickhouse-client --user defaul \
                  --host <SERVER_HOST> \
                  --port 9004
```

Verify the gcs storage
```sql
SELECT * FROM system.disks FORMAT Vertical
```

## Link
* [Clickhouse installation reference](https://clickhouse.com/docs/en/install#self-managed-install)
* [Network ports](https://clickhouse.com/docs/en/guides/sre/network-ports/)
* [GCS Replication](https://clickhouse.com/docs/en/guides/sre/gcs-multi-region/)
* [User Config](https://clickhouse.uptrace.dev/clickhouse/configuration.html#how-to-configure-clickhouse)