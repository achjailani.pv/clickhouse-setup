#!/bin/sh

# copy template file
#mv generated_files/custom_storage.xml /etc/clickhouse-server/config.d/
#mv generated_files/custom_users.xml /etc/clickhouse-server/users.d/

# change file owner
chown clickhouse:clickhouse /etc/clickhouse-server/config.d/custom_storage.xml
chown clickhouse:clickhouse /etc/clickhouse-server/users.d/custom_users.xml
