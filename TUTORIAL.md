# Clickhouse Kickoff
This page will walk us through how to test **Clickhouse** with `GCP Instance` and `GCS` that we have already created [here](README.md)

> The implementation we're reading here is in on local development

* Connect to clickhouse server
```shell
clickhouse-client --user default \
                  --password
```

* Verify disk

```sql
Row 1:
──────
name:             cache
path:             /var/lib/clickhouse/disks/gcs/
free_space:       18446744073709551615
total_space:      18446744073709551615
unreserved_space: 18446744073709551615
keep_free_space:  0
type:             s3
is_encrypted:     0
is_read_only:     0
is_write_once:    0
is_remote:        1
is_broken:        0
cache_path:       /var/lib/clickhouse/disks/gcs_cache/

Row 2:
──────
name:             default
path:             /var/lib/clickhouse/
free_space:       5265514496
total_space:      10331889664
unreserved_space: 5265514496
keep_free_space:  0
type:             local
is_encrypted:     0
is_read_only:     0
is_write_once:    0
is_remote:        0
is_broken:        0
cache_path:       

Row 3:
──────
name:             gcs
path:             /var/lib/clickhouse/disks/gcs/
free_space:       18446744073709551615
total_space:      18446744073709551615
unreserved_space: 18446744073709551615
keep_free_space:  0
type:             s3
is_encrypted:     0
is_read_only:     0
is_write_once:    0
is_remote:        1
is_broken:        0
cache_path:      
```

* Create database
```sql
CREATE database ch_carstensz
```
* Show database
```sql
SHOW databases
```
You should see the following databases
```sql
┌─name───────────────┐
│ INFORMATION_SCHEMA │
│ default            │
| ch_carstensz       |
│ information_schema │
│ system             │
└────────────────────┘
```

* Use the database we've created
```sql
USE ch_carstensz
```

* Create table
```sql
CREATE TABLE http_logs
(
    id Int64,
    env String NOT NULL
    `event` String,
    service String NOT NULL,
    message String,
    protocol String
    created_at DateTime,
)
ENGINE = MergeTree()
PRIMARY KEY (id)
SETTINGS storage_policy='gcs_main'
```
The creation table above, we set `SETTINGS storage_policy='gcs_main'` which means using `GCS` policy 

* Show tables
```sql
SHOW tables
```
You should the following result
```sql
┌─name──────┐
│ http_logs │
└───────────┘
```

Let's take a look the table fields
```sql
DESCRIBE 
```
```sql
┌─name───────┬─type─────┬─default_type─┬─default_expression─┬─comment─┬─codec_expression─┬─ttl_expression─┐
│ id         │ Int64    │              │                    │         │                  │                │
│ env        │ String   │              │                    │         │                  │                │
│ event      │ String   │              │                    │         │                  │                │
│ service    │ String   │              │                    │         │                  │                │
│ message    │ String   │              │                    │         │                  │                │
│ protocol   │ String   │              │                    │         │                  │                │
│ created_at │ DateTime │              │                    │         │                  │                │
└────────────┴──────────┴──────────────┴────────────────────┴─────────┴──────────────────┴────────────────┘
```
Let's test by inserting data.

```sql
INSERT INTO http_logs (id, evn, message, `event`, service, protocol, created_at) VALUES
    (101, 'development', 'POST /checkout for 192.321.22.21',          '{"type":"liberty_accesslog", "host":"79e8ad2347b3",  "ibm_userDir":"\/opt\/ibm\/wlp\/usr\/", "ibm_remoteHost":"172.27.0.10", "ibm_requestProtocol":"HTTP\/1.1", "ibm_userAgent":"Apache-CXF/3.3.3-SNAPSHOT"}',        'Memento',              'HTTP' ,now() ),
    (102, 'development', 'Insert a lot of rows per batch',   '{"type":"liberty_accesslog", "host":"79e8ad2347b3",  "ibm_userDir":"\/opt\/ibm\/wlp\/usr\/", "ibm_remoteHost":"172.27.0.10", "ibm_requestProtocol":"HTTP\/1.1", "ibm_userAgent":"Apache-CXF/3.3.3-SNAPSHOT"}',          'Migrator',        'HTTP' ,yesterday()),
    (102, 'development', 'Sort your data based on your commonly-used queries', '{"type":"liberty_accesslog", "host":"79e8ad2347b3",  "ibm_userDir":"\/opt\/ibm\/wlp\/usr\/", "ibm_remoteHost":"172.27.0.10", "ibm_requestProtocol":"HTTP\/1.1", "ibm_userAgent":"Apache-CXF/3.3.3-SNAPSHOT"}', 'Memento', 'HTTP', today()),
    (101, 'development', 'BADAP0004W: BadApp Angry for test', '{"type":"liberty_accesslog", "host":"79e8ad2347b3",  "ibm_userDir":"\/opt\/ibm\/wlp\/usr\/", "ibm_remoteHost":"172.27.0.10", "ibm_requestProtocol":"HTTP\/1.1", "ibm_userAgent":"Apache-CXF/3.3.3-SNAPSHOT"}',    'Memento', 'HTTP', now() + 5)
```
Show data
```sql
SELECT * from http_logs
```
You should see the following result
```sql
┌──id─┬─env─────────┬─event──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┬─service──┬─message────────────────────────────────────────────┬─protocol─┬──────────created_at─┐
│ 101 │ development │ {"type":"liberty_accesslog", "host":"79e8ad2347b3",  "ibm_userDir":"/opt/ibm/wlp/usr/", "ibm_remoteHost":"172.27.0.10", "ibm_requestProtocol":"HTTP/1.1", "ibm_userAgent":"Apache-CXF/3.3.3-SNAPSHOT"} │ Memento  │ POST /checkout for 192.321.22.21                   │ HTTP     │ 2023-03-06 17:38:41 │
│ 101 │ development │ {"type":"liberty_accesslog", "host":"79e8ad2347b3",  "ibm_userDir":"/opt/ibm/wlp/usr/", "ibm_remoteHost":"172.27.0.10", "ibm_requestProtocol":"HTTP/1.1", "ibm_userAgent":"Apache-CXF/3.3.3-SNAPSHOT"} │ Memento  │ BADAP0004W: BadApp Angry for test                  │ HTTP     │ 2023-03-06 17:38:46 │
│ 102 │ development │ {"type":"liberty_accesslog", "host":"79e8ad2347b3",  "ibm_userDir":"/opt/ibm/wlp/usr/", "ibm_remoteHost":"172.27.0.10", "ibm_requestProtocol":"HTTP/1.1", "ibm_userAgent":"Apache-CXF/3.3.3-SNAPSHOT"} │ Migrator │ Insert a lot of rows per batch                     │ HTTP     │ 2023-03-05 00:00:00 │
│ 102 │ development │ {"type":"liberty_accesslog", "host":"79e8ad2347b3",  "ibm_userDir":"/opt/ibm/wlp/usr/", "ibm_remoteHost":"172.27.0.10", "ibm_requestProtocol":"HTTP/1.1", "ibm_userAgent":"Apache-CXF/3.3.3-SNAPSHOT"} │ Memento  │ Sort your data based on your commonly-used queries │ HTTP     │ 2023-03-06 00:00:00 │
└─────┴─────────────┴────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┴──────────┴────────────────────────────────────────────────────┴──────────┴─────────────────────┘
```
To make sure the data we've added stored on gcs, open the bucket folder, you should see there

* Check valid JSON format
```sql
SELECT `event`, isValidJSON(`event`) FROM http_logs
```

* Check attribute validity
```sql
SELECT
    JSONHas(`event`, 'type') AND JSONHas(`event`, 'host') AS is_valid,
    count(*)
FROM http_logs GROUP BY is_valid
```
```sql
┌─is_valid─┬─count()─┐
│        1 │       4 │
└──────────┴─────────┘
```