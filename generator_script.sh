#!/bin/sh

# Will generate custom user template
cat > generated_files/custom_config.xml <<EOF
<clickhouse>
  <listen_host>0.0.0.0</listen_host>
  <timezone>${TZ}</timezone>
</clickhouse>
EOF

# encrypt user password
# password_double_sha1_hex=$($CH_PASSWORD | sha1sum | tr -d '-' | xxd -r -p | sha1sum | tr -d '-')

# Will generate custom user template
cat > generated_files/custom_users.xml <<EOF
<clickhouse>
  <profiles>
    <default>
      <background_pool_size>16</background_pool_size>
      <prefer_column_name_to_alias>1</prefer_column_name_to_alias>
    </default>
  </profiles>

  <users>
    <default>
      <password_sha256_hex></password_sha256_hex>
    </default>
  </users>
</clickhouse>
EOF

# Will generate custom storage template
cat > generated_files/custom_storage.xml <<EOF
<clickhouse>
    <storage_configuration>
        <disks>
            <gcs>
                <support_batch_delete>false</support_batch_delete>
                <type>s3</type>
                <endpoint>https://storage.googleapis.com/</endpoint>
                <access_key_id></access_key_id>
                <secret_access_key></secret_access_key>
                <metadata_path>/var/lib/clickhouse/disks/gcs/</metadata_path>
            </gcs>
            <cache>
                <type>cache</type>
                <disk>gcs</disk>
                <path>/var/lib/clickhouse/disks/gcs_cache/</path>
                <max_size>10Gi</max_size>
            </cache>
        </disks>
        <policies>
            <gcs_main>
                <volumes>
                    <main>
                        <disk>gcs</disk>
                    </main>
                </volumes>
            </gcs_main>
        </policies>
    </storage_configuration>
</clickhouse>
EOF
